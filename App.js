import React, { useState, useEffect } from "react";
import { Text, TextInput } from "react-native";
import * as Font from "expo-font";
import AppContainer from "./src/navigation";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/es/integration/react";
import { store, persistor } from "./src/redux/rootConfig";
import * as Notifications from "expo-notifications";
import { getUser } from "./src/redux/actions/userAction";
import global from "./resources/global";
import { updateUser } from "./src/redux/actions/authAction";
// import * as SplashScreen from "expo-splash-screen";
// import { Asset } from "expo-asset";
import AnimatedSplash from "react-native-animated-splash-screen";
import "./resources/i18n";
Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

export default function App() {
  Text.defaultProps = {};
  Text.defaultProps.allowFontScaling = false;
  TextInput.defaultProps.allowFontScaling = false;
  const [loaded, setLoaded] = useState(false);
  const dispatch = store.dispatch;
  const getState = store.getState();
  const { token } = getState.auth;
  const { user } = getState.user;
  const getUserData = () => {
    if (token) {
      dispatch(getUser(token));
      if (user) {
        registerForPushNotificationsAsync(user.notification_token);
      }
    }
  };
  const registerForPushNotificationsAsync = async (settedToken) => {
    const {
      status: existingStatus,
    } = await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== "granted") {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== "granted") {
      return;
    }
    const notificationToken = await Notifications.getExpoPushTokenAsync();
    if (notificationToken != settedToken) {
      const body = {
        notification_token: notificationToken.data,
      };
      dispatch(updateUser(body, token));
    }
    if (Platform.OS === "android") {
      Notifications.setNotificationChannelAsync("default", {
        name: "kayvoni",
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: global.colors.main,
      });
    }
  };
  useEffect(() => {
    Font.loadAsync({
      Bold: require("./assets/fonts/bold.ttf"),
      Book: require("./assets/fonts/book.ttf"),
      SemiBold: require("./assets/fonts/semibold.ttf"),
    }).then(() => {
      getUserData();
      setLoaded(true);
    });
  }, []);
  return (
    <AnimatedSplash
      translucent={true}
      isLoaded={loaded}
      logoImage={global.images.loaderMain}
      backgroundColor={global.colors.main}
      logoHeight={300}
      logoWidth={300}
    >
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          {loaded && <AppContainer />}
        </PersistGate>
      </Provider>
    </AnimatedSplash>
  );
}
