import React, { useEffect, useState } from "react";
import { Image, Text, ScrollView, TouchableOpacity, View } from "react-native";
import styled from "styled-components";
import { SemiBold14, Book16 } from "../../resources/palettes";
import global from "../../resources/global";
import CurrentOrdersBlock from "../components/CurrentOrdersBlock";
import HistoryOrderBlock from "../components/HistoryOrderBlock";
import { useDispatch, useSelector } from "react-redux";
import { getUserOrders } from "../redux/actions/userOrdersAction";
import LoadingScreen from "./LoadingScreen";
import { StatusName, handleStatusColor } from "../redux/utils";
import { useTranslation } from "react-i18next";

const OrderHistory = ({ orders }) => (
  <>
    {orders.map((order, key) => (
      <HistoryOrderBlock
        key={`${order.id} ${key}`}
        id={order.id}
        name={order.restaurant.name}
        price={order.total_price}
        status={<StatusName status={order.status} />}
        statusColor={handleStatusColor(order.status)}
        image={order.restaurant.logo}
        rating={order.rating ? parseInt(order.rating.rate) : 0}
      />
    ))}
  </>
);
const CurrentOrders = ({ orders }) => (
  <>
    {orders.map((order, key) => (
      <CurrentOrdersBlock
        key={`${order.id} ${key}`}
        id={order.id}
        name={order.restaurant.name}
        price={order.payed_amount}
        status={<StatusName status={order.status} />}
        statusColor={handleStatusColor(order.status)}
        image={order.restaurant.logo}
      />
    ))}
  </>
);

export default function MyOrdersScreen() {
  const { t } = useTranslation();
  const [type, setType] = useState(0);

  const dispatch = useDispatch();
  const { token } = useSelector((state) => state.auth);
  const { userOrders, userOrdersLoading, userOrdersError } = useSelector(
    (state) => state.userOrders
  );
  useEffect(() => {
    dispatch(getUserOrders(token));
  }, [type]);
  const orderHistory = userOrders.filter(
    (item) => item.status == -1 || item.status == -2 || item.status == 3
  );
  const orderActive = userOrders.filter(
    (item) => item.status == 1 || item.status == 2
  );
  if (userOrdersLoading) return <LoadingScreen />;
  if (userOrdersError) return <Text> {userOrdersError.message} </Text>;
  return (
    <ScrollView style={{ backgroundColor: global.colors.white }}>
      <OrdersWrap>
        <OrdersButtonWrap>
          <OrdersButton
            color={type != 1 ? global.colors.white : global.colors.main}
            onPress={() => type != 1 && setType(1)}
          >
            <SemiBold14
              color={type != 1 ? global.colors.main : global.colors.white}
            >
              {t("orderHistory")}
            </SemiBold14>
          </OrdersButton>
          <OrdersButton
            onPress={() => type != 0 && setType(0)}
            color={type != 0 ? global.colors.white : global.colors.main}
          >
            <SemiBold14
              color={type != 0 ? global.colors.main : global.colors.white}
            >
              {t("activeOrder")}
            </SemiBold14>
          </OrdersButton>
        </OrdersButtonWrap>
        {type ? (
          <OrderHistory orders={orderHistory} />
        ) : (
          <CurrentOrders orders={orderActive} />
        )}
      </OrdersWrap>
      <View height={50} />
    </ScrollView>
  );
}

const OrdersWrap = styled.View`
  padding: 10px;
`;

const OrdersButtonWrap = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const OrdersButton = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  width: 49%;
  height: 40px;
  background-color: ${({ color = global.colors.blue }) => color};
  border-radius: 10px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.12);
  elevation: 5;
`;

const ReviewBlock = styled.View`
  margin-top: 10px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const StarsWrap = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 65%;
  height: 30px;
  background-color: ${global.colors.white};
  border-radius: 10px;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.12);
`;
