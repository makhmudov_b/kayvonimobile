import React, { useEffect, useState } from "react";
import {
  Image,
  ScrollView,
  TouchableOpacity,
  View,
  Text,
  Alert,
} from "react-native";
import global from "../../resources/global";
import {
  Bold14,
  Bold18,
  SemiBold14,
  SemiBold13,
  SemiBold12,
  Book12,
} from "../../resources/palettes";
import {
  CartWrap,
  CartTitle,
  CartInputWrap,
  VisitorsBlock,
  VisitorCount,
  FoodMenuWrap,
  PayButton,
} from "../../resources/styles";
import { CountBlock } from "../../resources/styles";
import Minus from "../../assets/img/minus.svg";
import Plus from "../../assets/img/plus.svg";
import Clock from "../../assets/img/clock.svg";
import FoodMenu from "../components/FoodMenu";
import CheckBlock from "../components/CheckBlock";
import { DateTimePickerBlock } from "../components/DateTimePickerBlock";
import { useDispatch, useSelector } from "react-redux";
import { changeStore, getCart } from "../redux/actions/cartAction";
import LoadingBlock from "../components/LoadingBlock";
import Input from "../components/Input";
import { useTranslation } from "react-i18next";
const today = new Date();
const getDay = today.getDay();
export default function CartScreen({ navigation }) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const {
    cart,
    restaurant,
    restaurant_id,
    cartLoading,
    cartError,
    seats_count,
    comment,
    reserved_time,
  } = useSelector((state) => state.cart);
  const { token } = useSelector((state) => state.auth);

  useEffect(() => {
    if (!!restaurant_id) {
      dispatch(getCart(restaurant_id, token));
    }
  }, []);
  const calculatePrice = () => {
    let price = 0;
    let items_price = 0;
    const {
      prepaid_percentage: rest_percentage,
      percentage: kayvoni_percentage,
      fee_per_seat,
      deposit,
    } = restaurant;
    if (cart.length) {
      items_price = cart.reduce(
        (acc, item) => acc + parseInt(item.count) * parseInt(item.params.price),
        0
      );
      price = items_price;
    }
    if (price < deposit) {
      price = parseInt(deposit);
    }
    const onePercentAmount = price / 100;
    price =
      parseFloat(kayvoni_percentage) * onePercentAmount +
      parseFloat(rest_percentage) * onePercentAmount +
      parseInt(fee_per_seat) * seats_count;
    return { prepaid: price, items_price };
  };
  const setStore = (key, value) => {
    dispatch(changeStore({ key, value }));
  };
  const setCounter = (value) => {
    setStore("seats_count", value);
  };
  const increment = () => {
    setCounter(seats_count + 1);
  };
  const decrement = () => {
    seats_count > 1 && setCounter(seats_count - 1);
  };

  const getPersonPrice = (price = 1000) => {
    return price * seats_count;
  };
  const handleDate = (value) => {
    setStore("reserved_time", value);
  };
  const getParsedTime = (time) => {
    return new Date(
      reserved_time.getFullYear(),
      reserved_time.getMonth(),
      reserved_time.getDate(),
      time.substring(0, 2),
      time.substring(3, 5)
    );
  };
  const handlePayment = () => {
    if (!reserved_time) {
      Alert.alert(t("getTime"));
      return;
    }
    const { open_time, close_time } = restaurant.workdays[
      reserved_time.getDay()
    ];
    const checker = getParsedTime(reserved_time.toTimeString());
    const from = getParsedTime(open_time);
    const to = getParsedTime(close_time);
    if (!(checker >= from && checker < to)) {
      Alert.alert(t("sorry"));
      return;
    }
    navigation.navigate("PayScreen");
  };
  if (cartLoading) return <LoadingBlock />;
  if (cartError) return <Text>{cartError.message}</Text>;
  if (!restaurant) {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          position: "relative",
          justifyContent: "flex-end",
        }}
      >
        <Image
          source={global.images.loaderMain}
          style={{
            position: "absolute",
            width: global.strings.width,
            resizeMode: "cover",
          }}
        />
        <View
          style={{
            backgroundColor: global.colors.main,
            borderRadius: 20,
            padding: 20,
            marginBottom: 30,
          }}
        >
          <Bold18>{t("emptyCart")}</Bold18>
        </View>
      </View>
    );
  }
  const calculated = calculatePrice();
  return (
    <>
      <ScrollView style={{ backgroundColor: global.colors.white }}>
        <CartWrap>
          <CartTitle>
            <Bold18 style={{ flex: 1 }} color={global.colors.textColor}>
              {restaurant && restaurant.name}
            </Bold18>
            <Image
              style={{
                width: 40,
                height: 40,
                resizeMode: "contain",
                borderRadius: 40,
                overflow: "hidden",
              }}
              source={{ uri: restaurant.logo }}
            />
          </CartTitle>
          <View
            style={{
              marginTop: 10,
              borderBottomColor: global.colors.textColor2,
              borderBottomWidth: 1,
              paddingVertical: 10,
              alignItems: "center",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            {restaurant && (
              <>
                <Book12 color={global.colors.main}>
                  {t("deposit")}: {restaurant.deposit} UZS
                </Book12>
                <View
                  style={{
                    alignItems: "center",
                    flexDirection: "row",
                  }}
                >
                  <Clock />
                  <View width={5} />
                  <Book12 color={global.colors.main}>
                    {restaurant.workdays[getDay].open_time.slice(0, -3)}
                    {" - "}
                  </Book12>
                  <Book12 color={global.colors.main}>
                    {restaurant.workdays[getDay].close_time.slice(0, -3)}
                  </Book12>
                </View>
              </>
            )}
          </View>
          <CartInputWrap>
            <SemiBold14 color={global.colors.main}>
              {t("getTimeDate")}
            </SemiBold14>
            <DateTimePickerBlock
              placeholder={t("dateTime")}
              action={handleDate}
            />
          </CartInputWrap>
          {!!cart.length && (
            <Input
              title={t("preference")}
              value={comment}
              placeholder={t("addText")}
              onChangeText={(value) => setStore("comment", value)}
            />
          )}
          <VisitorsBlock>
            <Book12 color={global.colors.textColor}>{t("persons")}</Book12>
            {restaurant && (
              <VisitorCount>
                <SemiBold12
                  style={{ marginLeft: 10 }}
                  color={global.colors.textColor}
                >
                  {getPersonPrice(restaurant.fee_per_seat)} UZS
                </SemiBold12>
                <CountBlock>
                  <TouchableOpacity
                    onPress={decrement}
                    style={{ marginRight: 10 }}
                  >
                    <Minus />
                  </TouchableOpacity>
                  <Bold14 color={global.colors.textColor}>{seats_count}</Bold14>
                  <TouchableOpacity
                    onPress={increment}
                    style={{ marginLeft: 10 }}
                  >
                    <Plus />
                  </TouchableOpacity>
                </CountBlock>
              </VisitorCount>
            )}
            <FoodMenuWrap>
              {cart.map((item, key) => (
                <FoodMenu key={`${item.id} ${key}`} getKey={key} food={item} />
              ))}
            </FoodMenuWrap>
          </VisitorsBlock>
          <View style={{ marginTop: 10 }}>
            <View
              style={{
                borderBottomWidth: 1,
                borderBottomColor: global.colors.textColor2,
                paddingVertical: 10,
              }}
            >
              {!!cart.length && (
                <CheckBlock
                  name={t("orderCost")}
                  price={calculated.items_price}
                />
              )}
            </View>
            <CheckBlock name={t("prepayment")} price={calculated.prepaid} />
          </View>
        </CartWrap>
      </ScrollView>
      <PayButton onPress={() => handlePayment()}>
        <SemiBold13>{t("getPay")}</SemiBold13>
      </PayButton>
    </>
  );
}
