import React, { useState, useEffect } from "react";
import {
  ScrollView,
  View,
  Image,
  TouchableOpacity,
  Text,
  Alert,
} from "react-native";
import global from "../../resources/global";
import Icon from "../../assets/img/icon.svg";
import {
  Book10,
  Book14,
  Bold14,
  Bold18,
  SemiBold14,
  Book18,
} from "../../resources/palettes";
import Star from "../../assets/img/star.svg";
import EmptyStar from "../../assets/img/mini-empty-star.svg";
import Clock from "../../assets/img/clock.svg";
import InfoButtonBlock from "../components/InfoButtonBlock";
import ButtonBlock from "../components/ButtonBlock";
import FoodBlock from "../components/FoodBlock";
import {
  RestScreenWrap,
  MinTitle,
  Title,
  About,
  Info,
  Buttons,
  ButtonWrapper,
  FixedButton,
} from "../../resources/styles";
import { useDispatch, useSelector } from "react-redux";
import { getRestaurant } from "../redux/actions/restaurantAction";
import LoadingBlock from "../components/LoadingBlock";
import { cartSwitch } from "../redux/actions/cartAction";
import { useTranslation } from "react-i18next";
import { TabActions } from "@react-navigation/native";
import SliderDynamic from "../components/SliderDynamic";
import MoveDriveButton from "../components/DriveButton";

export default function RestaurantScreen({ navigation, route }) {
  const { t, i18n } = useTranslation();
  const current_lang = i18n.language;
  const { id } = route.params;
  const [active, setActive] = useState(0);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getRestaurant(id));
  }, []);
  const { restaurant, restaurantLoading, restaurantError } = useSelector(
    (state) => state.restaurant
  );
  const { token } = useSelector((state) => state.auth);
  const { restaurant_id } = useSelector((state) => state.cart);

  const today = new Date();
  const weekday = today.getDay();
  const switchRestaurant = () => {
    dispatch(cartSwitch(restaurant.id));
    navigation.navigate("CartScreen");
  };
  const handleOrder = () => {
    if (!token) {
      const jumpToAction = TabActions.jumpTo("Profile");
      navigation.dispatch(jumpToAction);
      navigation.goBack();
      return;
    }
    if (!restaurant_id) {
      switchRestaurant();
      return;
    } else {
      Alert.alert(
        t("resetCart"),
        t("resum"),
        [
          {
            text: t("cancel"),
          },
          { text: t("confirm"), onPress: () => switchRestaurant() },
        ],
        { cancelable: false }
      );
    }
  };
  if (restaurantLoading || !restaurant) return <LoadingBlock />;
  if (restaurantError) return <Text> {restaurantError.message} </Text>;
  const arr = [1, 2, 3, 4, 5];
  const ratingArray = arr.slice(0, restaurant.rating.avg);
  const emptyArray = arr.slice(0, -ratingArray.length);
  const getFiltered = (foods) => {
    if (active == 0) return foods;
    return foods.filter((item) => item.category_id == active);
  };
  return (
    <View style={{ backgroundColor: global.colors.white, flex: 1 }}>
      <ScrollView
        style={global.colors.white}
        showsVerticalScrollIndicator={false}
      >
        <TouchableOpacity
          onPress={() => navigation.navigate("PromotionScreen", { id })}
          style={{
            position: "absolute",
            top: 0,
            right: 15,
            zIndex: 1,
          }}
        >
          <Icon width={30} height={50} resizeMode={"contain"} />
        </TouchableOpacity>
        <SliderDynamic slider={restaurant.banner} />
        <RestScreenWrap>
          <MinTitle>
            <Book14 color={global.colors.main}>
              {restaurant.sorts.map((item) => item[`name_${current_lang}`])}
            </Book14>
            <Book14 color={global.colors.textColor2}>
              {t("deposit")}: {restaurant.deposit} UZS
            </Book14>
          </MinTitle>
          <Title>
            <Bold18 style={{ maxWidth: 245 }} color={global.colors.textColor}>
              {restaurant.name}
            </Bold18>
            <Image
              style={{ width: 40, height: 40, borderRadius: 50 }}
              source={{ uri: restaurant.logo }}
            />
          </Title>
          <About>
            <View style={{ flexDirection: "row" }}>
              <Clock />
              <Book10 style={{ marginLeft: 5 }} color={global.colors.textColor}>
                {restaurant.workdays[weekday].open_time.slice(0, -3)} -
                {restaurant.workdays[weekday].close_time.slice(0, -3)}
              </Book10>
            </View>

            <View style={{ flexDirection: "row", alignItems: "center" }}>
              {!!ratingArray &&
                ratingArray.map((i) => (
                  <Star key={i} style={{ marginRight: 5 }} />
                ))}
              {!!emptyArray &&
                emptyArray.map((i) => (
                  <EmptyStar key={i} style={{ marginRight: 5 }} />
                ))}
              <Book10 color={global.colors.textColor}>
                {`(${restaurant.rating.count})`}
              </Book10>
            </View>
          </About>
          <Book14
            style={{ maxWidth: 300, marginTop: 10 }}
            color={global.colors.textColor}
          >
            {restaurant[`description_${current_lang}`]}
          </Book14>
          <MoveDriveButton
            lat={restaurant.lat}
            name={restaurant.name}
            long={restaurant.long}
          />
          <Bold14 style={{ marginTop: 10 }} color={global.colors.textColor}>
            {t("additionalInfo")}
          </Bold14>
          <Info>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{ paddingVertical: 5 }}
            >
              {restaurant.types &&
                restaurant.types.map((info, key) => {
                  return (
                    <InfoButtonBlock
                      key={`${info.id}${key}`}
                      name={info[`name_${current_lang}`]}
                      image={info.image}
                    />
                  );
                })}
            </ScrollView>
          </Info>
          <Bold14 style={{ marginTop: 10 }} color={global.colors.textColor}>
            {t("categories")}
          </Bold14>
          <Buttons>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{ paddingVertical: 5 }}
            >
              <ButtonBlock
                color={active == 0 ? global.colors.main : global.colors.white}
                nameColor={
                  active == 0 ? global.colors.white : global.colors.textColor
                }
                name={t("all")}
                onPress={() => setActive(0)}
              />
              {restaurant.categories &&
                restaurant.categories.map((el, key) => {
                  return (
                    <ButtonBlock
                      color={
                        active == el.id
                          ? global.colors.main
                          : global.colors.white
                      }
                      key={`${el.id}${key}`}
                      nameColor={
                        active == el.id
                          ? global.colors.white
                          : global.colors.textColor
                      }
                      name={el[`name_${current_lang}`]}
                      onPress={() => setActive(el.id)}
                    />
                  );
                })}
            </ScrollView>
          </Buttons>
          {restaurant.food &&
            getFiltered(restaurant.food).map((food, key) => {
              return <FoodBlock key={`${food.id}${key}`} food={food} />;
            })}
        </RestScreenWrap>
      </ScrollView>
      <ButtonWrapper>
        <FixedButton onPress={handleOrder}>
          <SemiBold14>{t("tableBook")}</SemiBold14>
        </FixedButton>
      </ButtonWrapper>
    </View>
  );
}
