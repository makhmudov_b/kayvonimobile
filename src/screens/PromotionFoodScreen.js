import React, { useEffect } from "react";
import { Image, ScrollView, Text, View } from "react-native";
import global from "../../resources/global";
import { Bold18, Book12 } from "../../resources/palettes";
import FoodBlock from "../components/FoodBlock";
import { PromFoodWrap } from "../../resources/styles";
import { useDispatch, useSelector } from "react-redux";
import { getPromoList } from "../redux/actions/promoListAction";
import { useRoute } from "@react-navigation/native";
import LoadingBlock from "../components/LoadingBlock";
import { useTranslation } from "react-i18next";

export default function PromotionFoodScreen() {
  const { t, i18n } = useTranslation();
  const current_lang = i18n.language;
  const route = useRoute();
  const { promoList, promoListLoading, promoListError } = useSelector(
    (state) => state.promoList
  );
  const dispatch = useDispatch();
  const id = route.params.id;
  useEffect(() => {
    dispatch(getPromoList(id));
  }, []);
  if (promoListLoading) return <LoadingBlock />;
  if (promoListError) return <Text>{promoListError.message}</Text>;
  return (
    <View style={{ backgroundColor: global.colors.white, flex: 1 }}>
      <ScrollView style={{ backgroundColor: global.colors.white }}>
        <Image
          style={{ width: global.strings.width, height: 180 }}
          source={{ uri: promoList.image }}
        />
        <PromFoodWrap>
          <Bold18 color={global.colors.textColor}>
            {promoList[`name_${current_lang}`]}
          </Bold18>
          <Book12 style={{ marginTop: 10 }} color={global.colors.textColor}>
            {promoList[`description_${current_lang}`]}
          </Book12>
          <Bold18 style={{ marginTop: 10 }} color={global.colors.textColor}>
            {t("promoFood")}
          </Bold18>
          {promoList.foods &&
            promoList.foods.map((food, key) => {
              return <FoodBlock food={food} key={`${food.id}${key}`} />;
            })}
        </PromFoodWrap>
      </ScrollView>
    </View>
  );
}
