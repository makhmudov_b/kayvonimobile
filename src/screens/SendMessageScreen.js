import React, { useEffect, useState } from "react";
import {
  TouchableOpacity,
  ImageBackground,
  KeyboardAvoidingView,
  Platform,
  Alert,
} from "react-native";
import global from "../../resources/global";
import Phone from "../../assets/img/phone.svg";
import { Book13, Book14, SemiBold13 } from "../../resources/palettes";
import { TextInputMask } from "react-native-masked-text";
import {
  ProfileWrap,
  IconWrapp,
  InputWrapp,
  TextWrapp,
  SubmitButton,
} from "../../resources/styles";
import { useDispatch, useSelector } from "react-redux";
import {
  handleChangeAuth,
  verificationUser,
} from "../redux/actions/authAction";
import LoadingBlock from "../components/LoadingBlock";
import { useTranslation } from "react-i18next";

export default function SendMessageScreen({ navigation }) {
  const { t } = useTranslation();
  const [phone, setPhone] = useState("");

  const dispatch = useDispatch();

  const { loading, error } = useSelector((state) => state.auth);
  const routing = () => navigation.navigate("Confirm");
  const getCode = () => {
    if (phone.length === 13) {
      dispatch(verificationUser(phone.replace(/\D/g, ""), routing()));
    } else {
      Alert.alert(t("notEnoughtWarn"));
    }
  };
  useEffect(() => {
    if (loading) {
      dispatch(handleChangeAuth({ key: "loading", value: false }));
    }
  }, []);
  if (loading) return <LoadingBlock />;
  return (
    <KeyboardAvoidingView
      contentContainerStyle={{
        backgroundColor: "#999",
        flex: 1,
      }}
      behavior={Platform.OS == "android" ? "height" : "padding"}
    >
      <ProfileWrap>
        <ImageBackground
          style={{
            width: "100%",
            height: "100%",
            reseizeMode: "cover",
            justifyContent: "center",
          }}
          source={global.images.profileBg}
        >
          <IconWrapp>
            <Phone />
          </IconWrapp>
          <InputWrapp>
            <Book13 style={{ textAlign: "center" }}>{t("phoneNumber")}</Book13>
            <TextInputMask
              type="custom"
              options={{
                mask: "(99)999-99-99",
              }}
              placeholder="(99)999-99-99"
              placeholderTextColor={global.colors.textColor2}
              keyboardType={`phone-pad`}
              returnKeyType={"done"}
              value={phone}
              color={global.colors.main}
              onChangeText={(text) => setPhone(text)}
              maxLength={13}
              style={{
                width: "100%",
                height: 40,
                backgroundColor: global.colors.white,
                marginTop: 5,
                borderRadius: 10,
                padding: 10,
                textAlign: "center",
              }}
            />
          </InputWrapp>
          <TextWrapp>
            {error && <Book14>{error.message}</Book14>}
            <Book14>{t("accountInfo")}</Book14>
            <TouchableOpacity>
              <Book14 color={global.colors.lightYellow}>
                {t("accountInfo2")}
              </Book14>
            </TouchableOpacity>
          </TextWrapp>
          <SubmitButton onPress={() => getCode()}>
            {/* <SubmitButton onPress={() => navigation.navigate("Confirm")}> */}
            <SemiBold13>{t("getCode")}</SemiBold13>
          </SubmitButton>
        </ImageBackground>
      </ProfileWrap>
    </KeyboardAvoidingView>
  );
}
