import React from "react";
import { View, Image } from "react-native";
import global from "../../resources/global";

export default function LoadingScreen() {
  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "rgba(68,0,0,.8)",
      }}
    >
      <View
        style={{
          backgroundColor: global.colors.sand,
          width: 250,
          height: 250,
          alignItems: "center",
          justifyContent: "center",
          borderRadius: 250,
        }}
      >
        <Image
          source={global.images.loaderGif}
          style={{ resizeMode: "contain", width: "90%", height: "290%" }}
        />
      </View>
    </View>
  );
}
