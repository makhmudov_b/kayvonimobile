import React, { useEffect } from "react";
import { ScrollView, Text } from "react-native";
import global from "../../resources/global";
import PromotionBlock from "../components/PromotionBlock";
import { PromotionWrapp } from "../../resources/styles";
import { useDispatch, useSelector } from "react-redux";
import { getPromoList } from "../redux/actions/promoAction";
import { useRoute } from "@react-navigation/native";
import LoadingBlock from "../components/LoadingBlock";

export default function PromotionScreen() {
  const route = useRoute();
  const dispatch = useDispatch();
  const { promo, promoLoading, promoError } = useSelector(
    (state) => state.promo
  );
  const id = route.params.id;
  useEffect(() => {
    dispatch(getPromoList(id));
  }, []);
  if (promoLoading) return <LoadingBlock />;
  if (promoError) return <Text>{promoError.message}</Text>;
  return (
    <ScrollView style={{ backgroundColor: global.colors.white }}>
      <PromotionWrapp>
        {promo &&
          promo.map((prom, key) => {
            return <PromotionBlock promos={prom} key={`${prom.id} ${key}`} />;
          })}
      </PromotionWrapp>
    </ScrollView>
  );
}
