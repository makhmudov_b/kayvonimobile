import React, { useState } from "react";
import {
  ImageBackground,
  TextInput,
  KeyboardAvoidingView,
  Alert,
} from "react-native";
import global from "../../resources/global";
import Phone from "../../assets/img/phone.svg";
import { Book13, Book14, SemiBold13 } from "../../resources/palettes";
import {
  ProfileWrap,
  IconWrapp,
  InputWrapp,
  TextWrapp,
  SubmitButton,
} from "../../resources/styles";
import { useDispatch, useSelector } from "react-redux";
import { authorizeUser } from "../redux/actions/authAction";
import LoadingBlock from "../components/LoadingBlock";
import { useTranslation } from "react-i18next";

export default function SendMessageScreen({ navigation }) {
  const { t } = useTranslation();
  const [confirm, setConfirm] = useState("");

  const dispatch = useDispatch();

  const { loading, error, phone } = useSelector((state) => state.auth);
  const getRegister = () => {
    if (confirm.length === 4) {
      dispatch(
        authorizeUser(phone, confirm, () => navigation.replace("Register"))
      );
    } else {
      Alert.alert(t("notEnoughtWarn"));
    }
  };
  if (loading) return <LoadingBlock />;
  return (
    <KeyboardAvoidingView
      contentContainerStyle={{
        backgroundColor: "#999",
        flex: 1,
      }}
      behavior={Platform.OS == "android" ? "height" : "padding"}
    >
      <ProfileWrap>
        <ImageBackground
          style={{
            width: "100%",
            height: "100%",
            reseizeMode: "cover",
            justifyContent: "center",
          }}
          source={global.images.profileBg}
        >
          <IconWrapp>
            <Phone />
          </IconWrapp>
          <InputWrapp>
            <Book13 style={{ textAlign: "center" }}>{t("enterCode")}</Book13>
            <TextInput
              keyboardType={`phone-pad`}
              returnKeyType={"done"}
              value={confirm}
              color={global.colors.main}
              maxLength={4}
              onChangeText={(text) => setConfirm(text)}
              style={{
                width: "100%",
                height: 40,
                backgroundColor: global.colors.white,
                marginTop: 5,
                borderRadius: 10,
                padding: 10,
                textAlign: "center",
              }}
            />
          </InputWrapp>
          <TextWrapp>
            {error && <Book14>{error.message}</Book14>}
            <Book14>{t("sentCode")}</Book14>
            <Book14>
              {t("to")} +998 {phone}
            </Book14>
          </TextWrapp>
          <SubmitButton onPress={() => getRegister()}>
            {/* <SubmitButton onPress={() => navigation.navigate("Register")}> */}
            <SemiBold13>{t("confirmCode")}</SemiBold13>
          </SubmitButton>
        </ImageBackground>
      </ProfileWrap>
    </KeyboardAvoidingView>
  );
}
