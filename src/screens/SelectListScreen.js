import React, { useState } from "react";
import { ScrollView } from "react-native";
import global from "../../resources/global";
import { SemiBold13 } from "../../resources/palettes";
import CheckBtn from "../components/CheckBtn";
import { ListWrap, SelectButton } from "../../resources/styles";

export default function SelectListScreen() {
  const checkCategory = [
    {
      name: "Все",
      id: 0,
    },
    {
      name: "Азиатская еда",
      id: 1,
    },
    {
      name: "Европейская еда",
      id: 2,
    },
    {
      name: "Завтрак",
      id: 3,
    },
    {
      name: "Национальная еда",
      id: 4,
    },
    {
      name: "Азиатская еда",
      id: 5,
    },
  ];
  const [active, setActive] = useState(0);
  const checkButton = (elem) => {
    setActive(elem.id);
  };
  return (
    <>
      <ScrollView style={{ backgroundColor: global.colors.white }}>
        <ListWrap>
          {checkCategory.length > 0 &&
            checkCategory.map((el) => {
              return (
                <CheckBtn
                  color={
                    active == el.id ? global.colors.main : global.colors.white
                  }
                  key={el + el.id}
                  nameColor={
                    active == el.id
                      ? global.colors.white
                      : global.colors.textColor
                  }
                  name={el.name}
                  onPress={() => setActive(el.id)}
                />
              );
            })}
        </ListWrap>
      </ScrollView>
      <SelectButton>
        <SemiBold13>Выбрать</SemiBold13>
      </SelectButton>
    </>
  );
}
