import React, { useState, useEffect } from "react";
import { ScrollView, Text } from "react-native";
import global from "../../resources/global";
import { SemiBold13 } from "../../resources/palettes";
import CheckBtn from "../components/CheckBtn";
import { ListWrap, SelectButton } from "../../resources/styles";
import { useDispatch, useSelector } from "react-redux";
import { getCategory } from "../redux/actions/categoryAction";
import { getType } from "../redux/actions/typeAction";
import LoadingBlock from "../components/LoadingBlock";
import { useTranslation } from "react-i18next";
export default function SelectListScreen({ navigation, route }) {
  const { t, i18n } = useTranslation();
  const current_lang = i18n.language;
  const dispatch = useDispatch();
  const [chosen, setChosen] = useState([]);
  const { choiceType, setter, chosenData } = route.params;
  const { type, typeLoading, typeError } = useSelector((state) => state.type);

  useEffect(() => {
    if (chosenData.length) setChosen(chosenData);
    dispatch(getType(choiceType));
  }, []);
  if (typeLoading) return <LoadingBlock />;
  const data = type;
  const handleChoice = () => {
    setter(chosen);
    navigation.goBack();
  };
  const handleCheckBox = (data, checked) => {
    if (!checked) setChosen([...chosen, data]);
    else setChosen(chosen.filter((item) => item.id !== data.id));
  };
  return (
    <>
      <ScrollView style={{ backgroundColor: global.colors.white }}>
        <ListWrap>
          {!!data.length &&
            data.map((choice, key) => {
              const checked = !!chosen.find((item) => item.id === choice.id);
              return (
                <CheckBtn
                  key={key}
                  nameColor={
                    checked ? global.colors.white : global.colors.textColor
                  }
                  color={checked ? global.colors.main : global.colors.white}
                  name={choice[`name_${current_lang}`]}
                  onPress={() => handleCheckBox(choice, checked)}
                />
              );
            })}
        </ListWrap>
      </ScrollView>
      <SelectButton onPress={() => handleChoice()}>
        <SemiBold13>{t("getSelect")}</SemiBold13>
      </SelectButton>
    </>
  );
}
