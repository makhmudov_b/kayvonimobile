import React, { useEffect } from "react";
import { ImageBackground, Text, View } from "react-native";
import styled from "styled-components";
import global from "../../resources/global";
import {
  SemiBold18,
  Book18,
  Book14,
  Book12,
  SemiBold14,
} from "../../resources/palettes";
import ArrowRight from "../../assets/img/arrow-right.svg";
import SettingIcon from "../../assets/img/setting-icon.svg";
import { IconWrapp, ProfileWrap } from "../../resources/styles";
import Logo from "../../assets/img/logo.svg";
import Indev from "../../assets/img/indev.svg";
import Call from "../../assets/img/call-phone.svg";
import { useDispatch, useSelector } from "react-redux";
import { getUser } from "../redux/actions/userAction";
import LoadingScreen from "../screens/LoadingScreen";
import * as Linking from "expo-linking";
import { useTranslation } from "react-i18next";

export default function ProfileScreen({ navigation }) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { token } = useSelector((state) => state.auth);
  const { user, userLoading, userError } = useSelector((state) => state.user);
  useEffect(() => {
    dispatch(getUser(token));
  }, []);
  if (userLoading) return <LoadingScreen />;
  if (userError) return <Text>{userError.message}</Text>;
  return (
    <ProfileWrap>
      <ImageBackground
        style={{
          width: "100%",
          height: "100%",
          reseizeMode: "cover",
          justifyContent: "center",
          alignItems: "center",
        }}
        source={global.images.profileBg}
      >
        <IconWrapp>
          <Logo
            width={global.strings.width / 2}
            height={global.strings.width / 2}
          />
        </IconWrapp>
        <SemiBold18 style={{ marginTop: 10 }}> {user.name} </SemiBold18>
        <Book18 style={{ marginTop: 5 }}> +998 {user.phone} </Book18>
        <OrderButton
          style={{ paddingHorizontal: 10 }}
          onPress={() => navigation.navigate("MyOrdersScreen")}
        >
          <SemiBold14>{t("myOrders")}</SemiBold14>
          <ArrowRight />
        </OrderButton>
        <SettingsButton
          style={{ paddingHorizontal: 10 }}
          onPress={() => navigation.navigate("SettingScreen")}
        >
          <SemiBold14>{t("settings")}</SemiBold14>
          <SettingIcon />
        </SettingsButton>
        <SettingsButton
          style={{ paddingHorizontal: 10 }}
          onPress={() => Linking.openURL("tel:+998953405005")}
        >
          <SemiBold14>{t("support")}:</SemiBold14>
          <Call />
        </SettingsButton>
        <IndevWrap onPress={() => Linking.openURL("https://indev.uz")}>
          <Indev />
        </IndevWrap>
      </ImageBackground>
    </ProfileWrap>
  );
}

const IndevWrap = styled.TouchableOpacity`
  padding-top: 20px;
  align-items: center;
  justify-content: center;
  width: 100%;
`;
const OrderButton = styled.TouchableOpacity`
  margin-top: 30px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 40px;
  background-color: ${global.colors.main};
  border-style: solid;
  border-top-width: 1px;
  border-top-color: ${global.colors.white};
  border-bottom-width: 1px;
  border-bottom-color: ${global.colors.white};
`;

const SettingsButton = styled.TouchableOpacity`
  margin-top: 10px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 40px;
  background-color: ${global.colors.main};
  border-style: solid;
  border-top-width: 1px;
  border-top-color: ${global.colors.white};
  border-bottom-width: 1px;
  border-bottom-color: ${global.colors.white};
`;

const InfoBlock = styled.View`
  align-items: center;
  position: absolute;
  bottom: 30px;
`;
