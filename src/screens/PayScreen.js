import React from "react";
import { View, ScrollView } from "react-native";
import global from "../../resources/global";
import PayBlock from "../components/PayBlock";
import { useNavigation } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import { checkout } from "../redux/actions/cartAction";
import LoadingBlock from "../components/LoadingBlock";
import { formatDate } from "../redux/utils";
export default function PayScreen() {
  const navigation = useNavigation();
  const { token } = useSelector((state) => state.auth);
  const {
    cart,
    seats_count,
    cartLoading,
    reserved_time,
    comment,
    restaurant_id,
    cartError,
  } = useSelector((state) => state.cart);
  const dispatch = useDispatch();
  // PayCardScreen
  const handleCheckout = () => {
    const order_food = cart.map((item) => {
      return { food_id: item.id, param_id: item.params.id, count: item.count };
    });
    const data = {
      order_food: JSON.stringify(order_food),
      seats_count,
      reserve_time: formatDate(reserved_time),
      comment,
      restaurant_id,
    };
    const callback = (url) => {
      navigation.replace("WebScreen", { url });
    };
    dispatch(checkout(token, data, callback));
  };
  if (cartLoading) return <LoadingBlock />;
  if (cartError) return <Text>{cartError.message}</Text>;
  return (
    <View style={{ backgroundColor: global.colors.white }}>
      <View
        style={{
          padding: 10,
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <PayBlock action={handleCheckout} image={global.images.payMe} />
      </View>
    </View>
  );
}
