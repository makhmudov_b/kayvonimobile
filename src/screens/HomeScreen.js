import React, { useEffect } from "react";
import { View, Text, Image, TouchableOpacity, ScrollView } from "react-native";
import global from "../../resources/global";
import { Bold16 } from "../../resources/palettes";
import CategoryBlock from "../components/CategoryBlock";
import RestaurantBlock from "../components/RestaurantBlock";
import PromotionBlock from "../components/PromotionBlock";
import {
  Wrapper,
  PromotionsWrap,
  RestaurantWrap,
} from "../../resources/styles";
import Slider from "../components/Slider";
import { useDispatch, useSelector } from "react-redux";
import { getSliderList } from "../redux/actions/sliderAction";
import { fetchMainScreen } from "../redux/actions/mainActions";
import LoadingBlock from "../components/LoadingBlock";
import { useTranslation } from "react-i18next";

export default function HomeScreen() {
  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();
  const { language } = useSelector((state) => state.language);
  const getCurrentAppLang = i18n.language;
  const { main, homeScreenLoading, homeScreenError } = useSelector(
    (state) => state.main
  );
  useEffect(() => {
    if (language != getCurrentAppLang) {
      i18n.changeLanguage(language);
    }
    dispatch(fetchMainScreen());
    dispatch(getSliderList());
  }, []);

  if (homeScreenLoading || !main) return <LoadingBlock />;
  if (homeScreenError) return <Text>{homeScreenError.message}</Text>;
  const { categories, restaurant, promos } = main;
  return (
    <>
      <ScrollView style={{ backgroundColor: global.colors.white }}>
        <Slider />
        <Wrapper>
          {categories &&
            categories.map((category, key) => {
              return (
                <CategoryBlock
                  key={`${category.id} category ${key}`}
                  name={category[`name_${getCurrentAppLang}`]}
                  image={category.image}
                  color={category.color}
                  id={category.id}
                />
              );
            })}
          <CategoryBlock
            name={t("all")}
            image={global.images.category}
            color={"3E623E"}
            id={0}
          />
        </Wrapper>

        <Bold16 style={{ paddingLeft: 20 }} color={global.colors.textColor}>
          {t("recommend")}
        </Bold16>
        <View height={25} />
        <RestaurantWrap>
          {restaurant &&
            restaurant.map((rest, key) => {
              return (
                <RestaurantBlock
                  key={`${rest.id} restaurant ${key}`}
                  restaurant={rest}
                />
              );
            })}
        </RestaurantWrap>
        <Bold16 style={{ paddingLeft: 20 }} color={global.colors.textColor}>
          {t("sale")}
        </Bold16>
        <PromotionsWrap>
          {promos &&
            promos.map((prom, key) => {
              return (
                <PromotionBlock
                  key={`${prom.id} promos ${key}`}
                  promos={prom}
                />
              );
            })}
        </PromotionsWrap>
      </ScrollView>
    </>
  );
}
