import React, { useState, useEffect } from "react";
import {
  Image,
  ScrollView,
  TouchableOpacity,
  Text,
  View,
  Alert,
} from "react-native";
import global from "../../resources/global";
import { Book12, Bold18, Bold14, SemiBold14 } from "../../resources/palettes";
import Minus from "../../assets/img/minus.svg";
import Plus from "../../assets/img/plus.svg";
import SortButton from "../components/SortButton";
import {
  FoodScreenWrap,
  ImgWrap,
  PriceBlock,
  CountBlock,
  FoodButtons,
  FoodButtonWrapper,
  ToCart,
} from "../../resources/styles";
import { useDispatch, useSelector } from "react-redux";
import { getFood } from "../redux/actions/foodAction";
import LoadingBlock from "../components/LoadingBlock";
import { cartAdd } from "../redux/actions/cartAction";
import ModalBlock from "../components/ModalBlock";
import { useTranslation } from "react-i18next";
import { TabActions } from "@react-navigation/native";
export default function FoodScreen({ route, navigation }) {
  const { t, i18n } = useTranslation();
  const current_lang = i18n.language;
  const [count, setCount] = useState(1);
  const [active, setActive] = useState(0);
  const { id } = route.params;
  const { food, foodLoading, foodError } = useSelector((state) => state.food);
  const { token } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getFood(id));
  }, []);

  const getPrice = () => {
    const item = food.params.find((inner, i) => i == active);
    if (item) return item.price * count;
    return 0;
  };

  const increment = () => {
    setCount(count + 1);
  };
  const decrement = () => {
    if (count > 1) setCount(count - 1);
  };
  const [modal, setModal] = useState(false);
  const handleAdd = () => {
    if (!token) {
      const jumpToAction = TabActions.jumpTo("Profile");
      navigation.dispatch(jumpToAction);
      navigation.navigate("Home");
      return;
    }
    const food_data = { ...food, params: food.params[active], count };
    dispatch(cartAdd({ food: food_data }));
    setModal(true);
    setTimeout(() => {
      setModal(false);
    }, 1300);
  };

  // const price = getPrice() * count;
  if (foodLoading || !food) return <LoadingBlock />;
  if (foodError && !food) return <Text> {foodError.message} </Text>;
  return (
    <>
      <ScrollView style={{ backgroundColor: global.colors.white }}>
        <ImgWrap>
          <Image
            style={{ width: "100%", minHeight: 180, resizeMode: "cover" }}
            source={{ uri: food.image }}
          />
        </ImgWrap>
        <FoodScreenWrap>
          <Bold18
            style={{ maxWidth: 300, marginTop: 10 }}
            color={global.colors.textColor}
          >
            {food[`name_${current_lang}`]}
          </Bold18>
          <Book12 style={{ marginTop: 10 }} color={global.colors.textColor}>
            {food[`description_${current_lang}`]}
          </Book12>
          <PriceBlock>
            <Bold14 color={global.colors.textColor}>{getPrice()} UZS</Bold14>
            <CountBlock>
              <TouchableOpacity
                onPress={() => decrement()}
                style={{ marginRight: 10 }}
              >
                <Minus />
              </TouchableOpacity>
              <Bold14 color={global.colors.textColor}>{count}</Bold14>
              <TouchableOpacity
                onPress={() => increment()}
                style={{ marginLeft: 10 }}
              >
                <Plus />
              </TouchableOpacity>
            </CountBlock>
          </PriceBlock>
          <Bold18 style={{ marginTop: 25 }} color={global.colors.textColor}>
            {t("selectSort")}
          </Bold18>
          <FoodButtons>
            {!!food.params.length &&
              food.params.map((el, key) => {
                return (
                  <SortButton
                    color={
                      active == key ? global.colors.main : global.colors.white
                    }
                    key={`${el.id}${key}`}
                    nameColor={
                      active == key
                        ? global.colors.white
                        : global.colors.textColor
                    }
                    name={el[`name_${current_lang}`]}
                    price={`${el.price}`}
                    onPress={() => setActive(key)}
                  />
                );
              })}
          </FoodButtons>
        </FoodScreenWrap>
        <View height={100} />
      </ScrollView>
      <FoodButtonWrapper>
        <ToCart onPress={handleAdd}>
          <SemiBold14>{t("addToCard")}</SemiBold14>
        </ToCart>
      </FoodButtonWrapper>
      {modal && <ModalBlock title={t("successAdd")} />}
    </>
  );
}
