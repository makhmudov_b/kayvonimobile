import React, { useEffect } from "react";
import { ScrollView, Text } from "react-native";
import global from "../../resources/global";
import CategoryBlock from "../components/CategoryBlock";
import { CategoryWrapp } from "../../resources/styles";
import { useDispatch, useSelector } from "react-redux";
import { getCategory } from "../redux/actions/categoryAction";
import LoadingBlock from "../components/LoadingBlock";
import { useTranslation } from "react-i18next";

export default function CategoryScreen() {
  const { category, categoryScreenLoading, categoryScreenError } = useSelector(
    (state) => state.category
  );
  const { i18n } = useTranslation();
  const current_lang = i18n.language;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCategory());
  }, []);
  if ((categoryScreenLoading || !category) && !category.length)
    return <LoadingBlock />;
  if (categoryScreenError) return <Text>{error.message}</Text>;
  return (
    <ScrollView style={{ backgroundColor: global.colors.white }}>
      <CategoryWrapp>
        {category.map((cat, key) => {
          return (
            <CategoryBlock
              id={cat.id}
              key={`${cat.id}${key}`}
              name={cat[`name_${current_lang}`]}
              image={cat.image}
              color={cat.color}
            />
          );
        })}
      </CategoryWrapp>
    </ScrollView>
  );
}
