import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import global from "../resources/global";
import HomeScreen from "./screens/HomeScreen";
import Header from "./components/Header";
import CartScreen from "./screens/CartScreen";
import Home from "../assets/img/home.svg";
import Profile from "../assets/img/profile.svg";
import Search from "../assets/img/search.svg";
import Category from "../assets/img/category.svg";
import CategoryScreen from "./screens/CategoryScreen";
import ProfileScreen from "./screens/ProfileScreen";
import SearchScreen from "./screens/SearchScreen";
import { View } from "react-native";
import RestaurantScreen from "./screens/RestaurantScreen";
import RestCategory from "./screens/RestCategoryScreen";
import PromotionScreen from "./screens/PromotionScreen";
import PromotionFoodScreen from "./screens/PromotionFoodScreen";
import FoodScreen from "./screens/FoodScreen";
import PayScreen from "./screens/PayScreen";
import PayCardScreen from "./screens/PayCardScreen";
import PaidScreen from "./screens/PaidScreen";
import FilterScreen from "./screens/FilterScreen";
import SelectListScreen from "./screens/SelectListScreen";
import SendMessageScreen from "./screens/SendMessageScreen";
import ConfirmScreen from "./screens/ConfirmScreen";
import RegisterScreen from "./screens/RegisterScreen";
import MyOrdersScreen from "./screens/MyOrdersScreen";
import SettingScreen from "./screens/SettingScreen";
import OrderScreen from "./screens/OrderScreen";
import MapScreen from "./screens/MapScreen";
import SelectScreen from "./screens/SelectScreen";
import WebScreen from "./screens/WebScreen";

import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();
const ProfileAuth = createStackNavigator();
const AuthStack = () => {
  return (
    <ProfileAuth.Navigator headerMode={`none`}>
      <ProfileAuth.Screen name="Auth" component={SendMessageScreen} />
      <ProfileAuth.Screen name="Confirm" component={ConfirmScreen} />
      <ProfileAuth.Screen name="Register" component={RegisterScreen} />
    </ProfileAuth.Navigator>
  );
};
const AppContainer = () => {
  const { t } = useTranslation();
  // const [loading, setLoading] = useState(false);
  // const { registered, token, currentLang } = useContext(Context);
  const { token, name } = useSelector((state) => state.auth);
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          options={{
            header: () => <Header />,
          }}
        >
          {(navigation) => {
            return (
              <Tab.Navigator
                barStyle={{
                  height: 70,
                  backgroundColor: global.colors.main,
                }}
                initialRouteName={`Main`}
                shifting={false}
                labeled={false}
                activeColor={global.colors.main}
                inactiveColor={global.colors.white}
                backBehavior={`none`}
              >
                <Tab.Screen
                  options={{
                    tabBarIcon: ({ color }) => {
                      return (
                        <View
                          style={{
                            backgroundColor:
                              color === global.colors.main
                                ? global.colors.white
                                : global.colors.main,
                            padding: 7,
                            borderRadius: 50,
                          }}
                        >
                          <Home width={24} height={24} color={color} />
                        </View>
                      );
                    },
                  }}
                  name="Main"
                  component={HomeScreen}
                />
                <Tab.Screen
                  options={{
                    tabBarIcon: ({ color }) => (
                      <View
                        style={{
                          backgroundColor:
                            color === global.colors.main
                              ? global.colors.white
                              : global.colors.main,
                          padding: 7,
                          borderRadius: 50,
                        }}
                      >
                        <Category width={24} height={24} color={color} />
                      </View>
                    ),
                  }}
                  name="Category"
                  component={CategoryScreen}
                />
                <Tab.Screen
                  options={{
                    tabBarIcon: ({ color }) => (
                      <View
                        style={{
                          backgroundColor:
                            color === global.colors.main
                              ? global.colors.white
                              : global.colors.main,
                          padding: 7,
                          borderRadius: 50,
                        }}
                      >
                        <Profile width={24} height={24} color={color} />
                      </View>
                    ),
                  }}
                  name="Profile"
                  // component={ProfileScreen}
                  component={!!token && name.length ? ProfileScreen : AuthStack}
                />
                <Tab.Screen
                  options={{
                    tabBarIcon: ({ color }) => (
                      <View
                        style={{
                          backgroundColor:
                            color === global.colors.main
                              ? global.colors.white
                              : global.colors.main,
                          padding: 7,
                          borderRadius: 50,
                        }}
                      >
                        <Search width={24} height={24} color={color} />
                      </View>
                    ),
                  }}
                  name="Search"
                  component={SearchScreen}
                />
              </Tab.Navigator>
            );
          }}
        </Stack.Screen>
        <Stack.Screen
          name="Restaurant"
          options={{
            header: () => <Header title={t("restaurant")} enableBack={true} />,
          }}
          component={RestaurantScreen}
        />
        <Stack.Screen
          name="RestCategory"
          options={{
            header: () => <Header title={t("categories")} enableBack={true} />,
          }}
          component={RestCategory}
        />
        <Stack.Screen
          name="PromotionScreen"
          options={{
            header: () => <Header title={t("sale")} enableBack={true} />,
          }}
          component={PromotionScreen}
        />
        <Stack.Screen
          name="PromotionFoodScreen"
          options={{
            header: () => <Header title={t("sale")} enableBack={true} />,
          }}
          component={PromotionFoodScreen}
        />
        <Stack.Screen
          name="FoodScreen"
          options={{
            header: () => <Header title={t("food")} enableBack={true} />,
          }}
          component={FoodScreen}
        />
        <Stack.Screen
          name="CartScreen"
          options={{
            header: () => (
              <Header
                title={t("cart")}
                enableBack={true}
                disableCart={true}
                removeButton={true}
              />
            ),
          }}
          component={CartScreen}
        />
        <Stack.Screen
          name="PayScreen"
          options={{
            header: () => (
              <Header title={t("pay")} enableBack={true} disableCart={true} />
            ),
          }}
          component={PayScreen}
        />
        <Stack.Screen
          name="PayCardScreen"
          options={{
            header: () => (
              <Header title={"PAYME"} enableBack={true} disableCart={true} />
            ),
          }}
          component={PayCardScreen}
        />
        <Stack.Screen
          name="PaidScreen"
          options={{
            header: () => (
              <Header title={""} enableBack={true} disableCart={true} />
            ),
          }}
          component={PaidScreen}
        />
        <Stack.Screen
          name="FilterScreen"
          options={{
            header: () => <Header title={t("filter")} enableBack={true} />,
          }}
          component={FilterScreen}
        />
        <Stack.Screen
          name="SelectListScreen"
          options={{
            header: () => (
              <Header
                title={t("select")}
                enableBack={true}
                disableCart={true}
              />
            ),
          }}
          component={SelectListScreen}
        />
        <Stack.Screen
          name="MyOrdersScreen"
          options={{
            header: () => (
              <Header
                title={t("myOrders")}
                enableBack={true}
                disableCart={true}
              />
            ),
          }}
          component={MyOrdersScreen}
        />
        <Stack.Screen
          name="WebScreen"
          options={{
            header: () => (
              <Header
                title={t("payment")}
                enableBack={true}
                disableCart={true}
              />
            ),
          }}
          component={WebScreen}
        />
        <Stack.Screen
          name="SettingScreen"
          options={{
            header: () => (
              <Header
                title={t("settings")}
                enableBack={true}
                disableCart={true}
              />
            ),
          }}
          component={SettingScreen}
        />
        <Stack.Screen
          name="OrderScreen"
          options={{
            header: ({ scene }) => {
              return (
                <Header
                  title={t("order") + scene.route.params.id}
                  enableBack={true}
                  disableCart={true}
                />
              );
            },
          }}
          component={OrderScreen}
        />
        <Stack.Screen
          name="MapScreen"
          options={{
            header: () => <Header enableBack={true} disableCart={true} />,
          }}
          component={MapScreen}
        />
        <Stack.Screen
          name="SelectScreen"
          options={{
            header: () => (
              <Header
                title={t("select")}
                enableBack={true}
                disableCart={true}
              />
            ),
          }}
          component={SelectScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default AppContainer;
