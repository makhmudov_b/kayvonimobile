import React from "react";
import { Linking, View } from "react-native";
import { DriveButton } from "../../resources/styles";
import Discover from "../../assets/img/discover.svg";
import { Bold14 } from "../../resources/palettes";
import { useTranslation } from "react-i18next";
const MoveDriveButton = ({ lat, long, name }) => {
  const { t } = useTranslation();

  const openGps = () => {
    const scheme = Platform.select({
      ios: "maps:0,0?q=",
      android: "geo:0,0?q=",
    });
    const latLng = `${lat},${long}`;
    const label = name;
    const url = Platform.select({
      ios: `${scheme}${label}@${latLng}`,
      android: `${scheme}${latLng}(${label})`,
    });
    Linking.openURL(url);
  };
  return (
    <DriveButton onPress={openGps}>
      <Discover />
      <View width={10} />
      <Bold14>{t("drive")}</Bold14>
    </DriveButton>
  );
};
export default MoveDriveButton;
