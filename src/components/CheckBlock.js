import React from "react";
import { View } from "react-native";
import global from "../../resources/global";
import { Bold12, Book14 } from "../../resources/palettes";

export default function CheckBlock({ name, price, showSum = true }) {
  return (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 10,
      }}
    >
      <Bold12 color={global.colors.textColor}>{name}</Bold12>
      <Book14 color={global.colors.textColor}>
        {price} {showSum && "UZS"}
      </Book14>
    </View>
  );
}
