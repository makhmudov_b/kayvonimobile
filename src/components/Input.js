import React from "react";
import { View, TextInput } from "react-native";
import global from "../../resources/global";
import { SemiBold14 } from "../../resources/palettes";
import { FilterInputWrap } from "../../resources/styles";

export default function Input({ title, value, onChangeText, placeholder }) {
  return (
    <View style={{ marginTop: 5 }}>
      <SemiBold14 color={global.colors.main}>{title}</SemiBold14>
      <FilterInputWrap>
        <TextInput
          style={{
            fontFamily: global.fonts.book,
            fontSize: 14,
            height: 40,
            width: "100%",
            paddingHorizontal: 15,
            backgroundColor: "#fff",
            borderRadius: 10,
            overflow: "hidden",
          }}
          placeholder={placeholder}
          value={value}
          onChangeText={(text) => onChangeText(text)}
        />
      </FilterInputWrap>
    </View>
  );
}
