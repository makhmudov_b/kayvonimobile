import React from "react";
import { View, Image } from "react-native";
import CatBg from "../../assets/img/catBg.svg";
import { Book13, Book14 } from "../../resources/palettes";
import { useNavigation, TabActions } from "@react-navigation/native";
import { Category, CategoryLeft, CategoryRight } from "../../resources/styles";

export default function CategoryBlock({ name, image, color, id }) {
  const navigation = useNavigation();
  const jumpToAction = TabActions.jumpTo("Category");
  return (
    <Category
      color={color}
      onPress={() =>
        id > 0
          ? navigation.navigate("RestCategory", { id, name, color, image })
          : navigation.dispatch(jumpToAction)
      }
    >
      <CategoryLeft>
        <Book14>{name}</Book14>
      </CategoryLeft>
      <CategoryRight>
        <CatBg />
        <View
          style={{
            position: "absolute",
            bottom: 5,
            right: 0,
          }}
        >
          <Image
            style={{ width: 53, height: 53, resizeMode: "contain" }}
            source={id > 0 ? { uri: image } : image}
          />
        </View>
      </CategoryRight>
    </Category>
  );
}
