import React from "react";
import { SemiBold18 } from "../../resources/palettes";
import { Button } from "../../resources/styles";

export default function ButtonBlock({ name, color, nameColor, onPress }) {
  return (
    <Button
      color={color}
      style={{ paddingHorizontal: 10 }}
      onPress={() => onPress()}
    >
      <SemiBold18 color={nameColor}>{name}</SemiBold18>
    </Button>
  );
}
