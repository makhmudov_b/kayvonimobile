import React from "react";
import { SemiBold14 } from "../../resources/palettes";
import { SortButton } from "../../resources/styles";

export default function ButtonBlock({
  name,
  color,
  nameColor,
  price,
  onPress,
}) {
  return (
    <SortButton
      color={color}
      style={{ paddingHorizontal: 10 }}
      onPress={() => onPress()}
    >
      <SemiBold14 color={nameColor}>{name}</SemiBold14>
      <SemiBold14 color={nameColor}>{price} UZS</SemiBold14>
    </SortButton>
  );
}
