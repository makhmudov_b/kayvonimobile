import React from "react";
import { View, Image } from "react-native";
import global from "../../resources/global";
import { Book10 } from "../../resources/palettes";
import { InfoButtonBlock } from "../../resources/styles";

export default function InfoButton({ name, image }) {
  return (
    <InfoButtonBlock>
      <Image
        style={{
          width: 34,
          height: 34,
          resizeMode: "contain",
          borderRadius: 50,
          overflow: "hidden",
        }}
        source={{ uri: image }}
      />
      <View style={{ paddingHorizontal: 10 }}>
        <Book10 color={global.colors.textColor}>{name}</Book10>
      </View>
    </InfoButtonBlock>
  );
}
