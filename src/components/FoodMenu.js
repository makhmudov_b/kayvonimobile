import React from "react";
import { View, TouchableOpacity, Image } from "react-native";
import global from "../../resources/global";
import { Book12, Bold14 } from "../../resources/palettes";
import { FoodMenuBlock, CountBlock } from "../../resources/styles";
import Minus from "../../assets/img/minus.svg";
import Plus from "../../assets/img/plus.svg";
import RemoveFood from "../../assets/img/delete-button.svg";
import { useDispatch } from "react-redux";
import { cartRemove, cartUpdate } from "../redux/actions/cartAction";
import { useTranslation } from "react-i18next";

export default function FoodMenu({ food, getKey }) {
  const { i18n } = useTranslation();
  const current_lang = i18n.language;
  const { params, count, image } = food;
  const dispatch = useDispatch();

  function increment() {
    dispatch(cartUpdate(getKey, count + 1));
  }
  function decrement() {
    if (count > 1) dispatch(cartUpdate(getKey, count - 1));
    else dispatch(cartRemove(getKey));
  }
  const getFoodPrice = () => {
    return params.price * count;
  };
  return (
    <FoodMenuBlock>
      <Image
        source={{ uri: image }}
        style={{ borderRadius: 12.5, width: 25, height: 25 }}
      />
      <View style={{ flex: 1 }}>
        <Book12 style={{ marginLeft: 10 }} color={global.colors.textColor}>
          {food[`name_${current_lang}`]} ({params[`name_${current_lang}`]})
        </Book12>
      </View>
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Book12 style={{ marginRight: 20 }} color={global.colors.textColor}>
          {getFoodPrice()} UZS
        </Book12>
        <CountBlock>
          <TouchableOpacity onPress={decrement} style={{ marginRight: 10 }}>
            {count > 1 ? <Minus /> : <RemoveFood />}
          </TouchableOpacity>
          <Bold14 color={global.colors.textColor}>{count}</Bold14>
          <TouchableOpacity onPress={increment} style={{ marginLeft: 10 }}>
            <Plus />
          </TouchableOpacity>
        </CountBlock>
      </View>
    </FoodMenuBlock>
  );
}
