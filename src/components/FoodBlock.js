import React from "react";
import { View, Image } from "react-native";
import global from "../../resources/global";
import { Book10, Bold14 } from "../../resources/palettes";
import { useNavigation } from "@react-navigation/native";
import { Food, FoodImg } from "../../resources/styles";
import { useTranslation } from "react-i18next";

export default function FoodBlock({ food }) {
  const navigation = useNavigation();
  const { i18n } = useTranslation();
  const current_lang = i18n.language;
  const { image, id, params } = food;
  return (
    <Food onPress={() => navigation.navigate("FoodScreen", { id })}>
      <View>
        <Bold14 style={{ maxWidth: 165 }} color={global.colors.textColor}>
          {food[`name_${current_lang}`]}
        </Bold14>
        <Book10
          style={{ marginTop: 10, maxWidth: 180 }}
          color={global.colors.textColor2}
        >
          {food[`description_${current_lang}`].slice(0, 100)}
          {food[`description_${current_lang}`].length > 100 && " ..."}
        </Book10>
        <Bold14 style={{ marginTop: 15 }} color={global.colors.textColor}>
          {params.length > 0 && params[0].price} UZS
        </Bold14>
      </View>
      <View style={{ alignItems: "center", justifyContent: "center" }}>
        <FoodImg>
          <Image
            style={{ width: 80, height: 80, borderRadius: 50 }}
            source={{ uri: image }}
          />
        </FoodImg>
      </View>
    </Food>
  );
}
