import React, { useState } from "react";
import {
  View,
  Button,
  Platform,
  Text,
  TouchableOpacity,
  Modal,
  TouchableWithoutFeedback,
} from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import global from "../../resources/global";
import { Book14, SemiBold13 } from "../../resources/palettes";
import { FilterBtn } from "../../resources/styles";
import ArrowRight from "../../assets/img/arrowRight.svg";

const today = new Date();
export const DateTimePickerBlock = ({ action, placeholder }) => {
  const [date, setDate] = useState(today);
  const [dateChosen, setDateChosen] = useState(false);
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);
  const onChange = (event, selectedDate) => {
    if (selectedDate) {
      setDate(selectedDate);
    }
  };
  const onChangeAndroid = (event, selectedDate) => {
    setShow(false);
    if (selectedDate) {
      setDate(selectedDate);
    } else {
      return;
    }
    if (mode === "time") {
      action(selectedDate);
      setDateChosen(true);
    } else {
      setMode("time");
      setShow(true);
    }
  };
  const hideAll = () => {
    if (mode === "time") {
      action(date);
      setDateChosen(true);
    }
    setMode("date");
    setShow(false);
  };
  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };

  const showTimepicker = () => {
    showMode("time");
  };
  const showAll = () => {
    showDatepicker();
  };
  const addZero = (value) => {
    if (value < 10) {
      return "0" + value;
    }
    return value;
  };
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minutes = date.getMinutes();
  const chosenDateTime = `${year}-${addZero(month)}-${day} ${addZero(
    hour
  )}:${addZero(minutes)}`;
  return (
    <View>
      <FilterBtn onPress={showAll} style={{ paddingHorizontal: 15 }}>
        <Book14
          color={
            dateChosen ? global.colors.textColor : global.colors.textColor2
          }
        >
          {dateChosen ? chosenDateTime : placeholder}
        </Book14>
        <ArrowRight />
      </FilterBtn>

      {Platform.OS == "ios" ? (
        <Modal visible={show} transparent={true} animationType={"fade"}>
          <View
            style={{
              flex: 1,
              justifyContent: "flex-end",
              backgroundColor: " rgba(0,0,0,.5)",
            }}
          >
            <View
              style={{
                backgroundColor: "white",
                paddingBottom: 20,
                paddingLeft: 20,
              }}
            >
              <View style={{ alignItems: "flex-end" }}>
                <TouchableOpacity
                  style={{
                    backgroundColor: global.colors.lightGreen,
                    borderRadius: 10,
                    paddingVertical: 8,
                    paddingHorizontal: 12,
                    marginTop: 10,
                    marginRight: 10,
                  }}
                  onPress={mode === "date" ? showTimepicker : hideAll}
                >
                  <SemiBold13>Выбрать</SemiBold13>
                </TouchableOpacity>
              </View>
              {show && (
                <View>
                  <DateTimePicker
                    value={date}
                    mode={mode}
                    minimumDate={today}
                    is24Hour={true}
                    display={"default"}
                    onChange={onChange}
                  />
                </View>
              )}
            </View>
          </View>
        </Modal>
      ) : (
        <>
          {show && (
            <DateTimePicker
              value={date}
              mode={mode}
              minimumDate={today}
              is24Hour={true}
              onChange={onChangeAndroid}
            />
          )}
        </>
      )}
    </View>
  );
};
