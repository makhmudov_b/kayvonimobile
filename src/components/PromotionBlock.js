import React from "react";
import { Image } from "react-native";
import global from "../../resources/global";
import { Bold14, Book12, Book10 } from "../../resources/palettes";
import Icon from "../../assets/img/icon.svg";
import { useNavigation } from "@react-navigation/native";
import {
  Promotion,
  PromotionLeft,
  PromotionCenter,
  PromotionRight,
} from "../../resources/styles";
import { useTranslation } from "react-i18next";

export default function PromotionBlock({ promos }) {
  const navigation = useNavigation();
  const { image } = promos;
  const { i18n } = useTranslation();
  const current_lang = i18n.language;
  return (
    <Promotion
      onPress={() =>
        navigation.navigate("PromotionFoodScreen", { id: promos.id })
      }
    >
      <PromotionLeft>
        <Bold14 color={global.colors.textColor}>
          {promos[`name_${current_lang}`]}
        </Bold14>
        <Book10
          style={{ maxWidth: 180, marginTop: 5 }}
          color={global.colors.textColor2}
        >
          {promos[`description_${current_lang}`].slice(0, 100)}
        </Book10>
      </PromotionLeft>
      <PromotionCenter>
        <Icon />
      </PromotionCenter>
      <PromotionRight>
        <Image
          source={{ uri: image }}
          style={{ width: 100, height: 100, borderRadius: 50 }}
        />
      </PromotionRight>
    </Promotion>
  );
}
