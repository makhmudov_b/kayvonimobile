import * as promoTypes from "../actionTypes/promoActionTypes";

const initialState = {
  promo: [],
  promoLoading: false,
  promoError: null,
};
export function promoReducer(state = initialState, { type, payload }) {
  switch (type) {
    case promoTypes.STARTING_PROMO:
      return { ...state, promoLoading: true };
    case promoTypes.SUCCESS_PROMO:
      return { ...state, promoLoading: false, promo: payload };
    case promoTypes.FAIL_PROMO:
      return {
        ...state,
        promoLoading: false,
        promoError: payload,
        promo: null,
      };
    default:
      return state;
  }
}
