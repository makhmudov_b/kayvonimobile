import * as typeTypes from "../actionTypes/typeActionTypes";

const initialState = {
  type: [],
  typeLoading: false,
  typeError: null,
};
export function typeReducer(state = initialState, { type, payload }) {
  switch (type) {
    case typeTypes.STARTING_TYPES:
      return { ...state, typeLoading: true };
    case typeTypes.SUCCESS_TYPES:
      return { ...state, typeLoading: false, type: payload };
    case typeTypes.FAIL_TYPES:
      return { ...state, typeLoading: false, typeError: payload, type: null };
    default:
      return state;
  }
}
