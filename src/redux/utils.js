import global from "../../resources/global";
import { useTranslation } from "react-i18next";
import moment from "moment";
export const updateState = (state, updated) => {
  return {
    ...state,
    ...updated,
  };
};

export const StatusName = ({ status }) => {
  const { t } = useTranslation();
  if (status == 1) return t("status1");
  if (status == 2) return t("status2");
  if (status == 3) return t("status3");
  if (status == -2) return t("status4");
  if (status == -3) return t("status5");
};
export const handleStatusColor = (status) => {
  if (status == 1) return global.colors.lightYellow;
  if (status == 2) return global.colors.lightGreen;
  if (status == 3) return global.colors.lightBlue;
  if (status == -2 || status == -3) return global.colors.red;
  return global.colors.lightBlue;
};
export const formatDate = (date) => {
  return moment(date).format("DD-MM-YYYY HH:mm");
};

export const numberPattern = /\d+/g;
