import ApiService from "../../services/api";
import * as sliderTypes from "../actionTypes/sliderActionTypes";

export const getSliderList = () => (dispatch) => {
  dispatch(sliderBegin());
  return ApiService.getResources("/slider", null)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(sliderSuccess(value.data));
      }
    })
    .catch((error) => dispatch(sliderFail(error)));
};

export function sliderBegin() {
  return {
    type: sliderTypes.STARTING_SLIDER,
  };
}

export function sliderSuccess(data) {
  return {
    type: sliderTypes.SUCCESS_SLIDER,
    payload: data,
  };
}

export function sliderFail(error) {
  return {
    type: sliderTypes.FAIL_SLIDER,
    payload: error,
  };
}
