import { Alert } from "react-native";
import ApiService from "../../services/api";
import * as cartTypes from "../actionTypes/cartActionTypes";
import { store } from "../rootConfig";
export const getCart = (restaurant_id, token) => (dispatch) => {
  dispatch(cartBegin());
  return ApiService.getResources(`/cart/restaurant/${restaurant_id}`, token)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(cartSuccess(value.data));
      }
    })
    .catch((error) => dispatch(cartFail(error)));
};

export const checkout = (token, data, callback) => (dispatch) => {
  dispatch(cartBegin());
  return ApiService.postEvent(`/user/checkout`, token, data)
    .then((value) => {
      if (value.statusCode === 200) {
        callback(value.data);
        dispatch(cartStop());
      } else {
        dispatch(cartStop());
      }
    })
    .catch((error) => dispatch(cartFail(error)));
};

function getCurrentRestaurantId() {
  return store.getState().cart.restaurant_id;
}
export function changeStore({ key, value }) {
  return {
    type: cartTypes.CHANGE_DATA_CART,
    payload: { key, value },
  };
}
export function cartBegin() {
  return {
    type: cartTypes.STARTING_CART,
  };
}
export function cartStop() {
  return {
    type: cartTypes.STOP_CART,
  };
}

export function checkoutSuccess(data) {
  return {
    type: cartTypes.SUCCESS_CHECKOUT,
    payload: data,
  };
}
export function cartSuccess(data) {
  return {
    type: cartTypes.SUCCESS_CART,
    payload: data,
  };
}

export function cartFail(error) {
  return {
    type: cartTypes.FAIL_CART,
    payload: error,
  };
}

//CART HANDLER
export const cartAdd = ({ food }) => (dispatch) => {
  const current_id = getCurrentRestaurantId();
  const restaurant_id = food.restaurant_id;
  if (!!restaurant_id && !!current_id)
    if (current_id != restaurant_id) {
      Alert.alert(
        "Можно заказывать только с одного ресторана",
        "Корзина будет очищена",
        [
          {
            text: "Отменить",
          },
          {
            text: "Продолжить",
            onPress: () => {
              dispatch(cartSwitch(restaurant_id));
              return dispatch({
                type: cartTypes.CART_ADD,
                payload: { food, restaurant_id },
              });
            },
          },
        ],
        { cancelable: false }
      );
      return;
    }
  return dispatch({
    type: cartTypes.CART_ADD,
    payload: { food, restaurant_id },
  });
};
export function cartSwitch(payload) {
  return {
    type: cartTypes.CART_SWITCH,
    payload,
  };
}
export function cartRemove(payload) {
  return {
    type: cartTypes.CART_REMOVE,
    payload,
  };
}
export function cartUpdate(key, count) {
  return {
    type: cartTypes.CART_UPDATE,
    payload: { key, count },
  };
}
export function cartClear() {
  return {
    type: cartTypes.CART_CLEAR,
  };
}
