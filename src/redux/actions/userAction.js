import ApiService from "../../services/api";
import * as userTypes from "../actionTypes/userActionType";

export const getUser = (token) => (dispatch) => {
  dispatch(userBegin());
  return ApiService.getResources("/user", token)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(userSuccess(value.data));
      }
    })
    .catch((error) => dispatch(userFail(error)));
};

export function userBegin() {
  return {
    type: userTypes.STARTING_USER,
  };
}

export function userSuccess(data) {
  return {
    type: userTypes.SUCCESS_USER,
    payload: data,
  };
}

export function userFail(error) {
  return {
    type: userTypes.FAIL_USER,
    payload: error,
  };
}
