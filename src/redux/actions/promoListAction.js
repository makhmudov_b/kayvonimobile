import ApiService from "../../services/api";
import * as promoListTypes from "../actionTypes/promoListActionTypes";

export const getPromoList = (id) => (dispatch) => {
  dispatch(promoListBegin());
  return ApiService.getResources(`/show/promo/${id}`, null)
    .then((value) => {
      if (value.statusCode === 200) {
        dispatch(promoListSuccess(value.data));
      }
    })
    .catch((error) => dispatch(promoListFail(error)));
};

export function promoListBegin() {
  return {
    type: promoListTypes.STARTING_PROMO_LIST,
  };
}

export function promoListSuccess(data) {
  return {
    type: promoListTypes.SUCCESS_PROMO_LIST,
    payload: data,
  };
}

export function promoListFail(error) {
  return {
    type: promoListTypes.FAIL_PROMO_LIST,
    payload: error,
  };
}
