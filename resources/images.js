const images = {
  profileBg: require("../assets/img/profileBg.png"),
  category: require("../assets/img/category.png"),
  payMe: require("../assets/img/payMe.png"),
  Star: require("../assets/img/bigStar.svg"),
  EmptyStar: require("../assets/img/empty-star.svg"),
  loaderGif: require("../assets/animation/Kayvoni-loader1.gif"),
  loaderMain: require("../assets/animation/Kayvoni-02.gif"),
};

export default images;
