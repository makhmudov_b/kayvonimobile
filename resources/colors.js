const colors = {
  main: "#440000",
  pink: "#FF5858",
  red: "#EB5757",
  white: "#FFFFFF",
  lightBlue: "#00A3FF",
  blue: "#634926",
  brown: "#450000",
  green: "#3E623E",
  lightGreen: "#6FCF97",
  yellow: "#EFA335",
  lightYellow: "#FFD600",
  textColor: "#333333",
  textColor2: "#BABABA",
  sand: "#FFE7C3",
};

export default colors;
